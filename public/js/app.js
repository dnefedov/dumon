'use strict'

angular.module('dumonApp', ['ngRoute', 'ui.select', 'ngSanitize'])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/dumon', {
            templateUrl: 'views/math.html'
        })
        .when('/dumon/result', {
            templateUrl: 'views/result.html'
        })
        .otherwise({
            redirectTo: '/dumon'
        });
}]);