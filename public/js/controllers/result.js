'use strict'

angular.module('dumonApp')
.controller('resultController', function($scope, $routeParams, $location, $timeout, mathService){
    this.answerShown = false;
    if (!mathService.request) {
        $location.path('dumon');
    }
    $scope.request = mathService.request;
    $scope.result = mathService.result;
    $scope.blueprint = mathService.blueprint;
    
    $scope.checkAnswer = function() {
        if (this.answerShown) {
            $('.answer').addClass('hidden');
            this.answerShown = false;
            $('.check_answer').text('Check answers');
            $scope.password = null;
        } else {
            if (!mathService.checkPsw($scope.password)) {
                alert('Do not mess with me, boy!');
                return;
            }
            $('.answer').removeClass('hidden');
            this.answerShown = true;
            $('.check_answer').text('Hide answers');
        }
    }

    $scope.back = function() {
        $location.path('/dumon');
    };
    $scope.print = function() {};
});