'use strict'

angular.module('dumonApp')
.controller('requestController', function($scope, $location, mathService){
    $scope.operations = ["+", "-", "*", "/"];
    $scope.request = mathService.request ? mathService.request : {parts : [{}]};

    $scope.calculate = function() {
        mathService.savePsw($scope.password);
        mathService.calculate($scope.request);
        $location.path('dumon/result');
    }

    $scope.add = function() {
        $scope.request.parts.push({});
    }

    $scope.remove = function (index) {
        $scope.request.parts.splice(index, 1);
    }

    $scope.clear = function(e) {
        $scope.request = {parts : [{}]};
    }
});