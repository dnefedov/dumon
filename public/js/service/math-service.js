angular.module('dumonApp')
.service('mathService', ['$http', '$q', function($http, $q) {

    var random = new Random(Random.engines.mt19937().autoSeed());
    
    this.calculate = function(request) {
        this.request = request;
        if (request.lines > 100) {
            console.warn('Too many lines ' + request.lines);
            request.lines = 100;
        }

        this.result = [];
        this.blueprint = populateBlueprint(request);
        for(i = 0; i < request.lines; i++) {
            var line = populateLine(request);

            this.result.push(line);
        }

        function populateBlueprint(request) {
            var blueprint = '[' + request.min + ', ' + request.max + ']';

            for (var i = 0; i < request.parts.length; i++) {
                var part = request.parts[i];
                blueprint = blueprint + ' [' + part.operations.toString() + ']';
                blueprint = blueprint + ' [' + part.min + ', ' + part.max + ']';
            }

            return blueprint;
        }

        function populateLine(request){
            var line = {};

            line['expression'] = populateExpression(request);
            line['result'] = calculateResult(clone(line['expression']));

            return line;
        }

        function clone(obj) {
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
            }
            return copy;
        }

        function populateExpression(request) {
            var expression = [];
            var firstNumber = getRandomInt(request.min, request.max);
            expression.push(firstNumber);

            for (var i = 0; i < request.parts.length; i++) {
                var part = request.parts[i];

                var operation = getRandomOperation(part.operations);
                var current = getRandomInt(part.min, part.max);

                // check for natural number (1, 2, 3, etc)
                // if fraction - increase left number
                if (part.naturalNumber && operation == '/') {
                    var previous = expression[expression.length - 1];
                    if (previous % current != 0) {
                        var diff = previous % current;
                        expression[expression.length - 1] += current - diff;
                    }
                }

                expression.push(operation);
                expression.push(current);

            }

            return expression;
        }

        function getRandomInt(min, max){
            return random.integer(min, max);
        }

        function getRandomOperation(operations) {
            if (operations == null) {
                return null;
            }
            var size = operations.length;
            if (size == 1){
                return operations[0];
            }
            var index = getRandomInt(0, size - 1);
            return operations[index];
        }

        function calculateResult(expression) {
            var pivot = Math.floor(expression.length / 2);

            //e.g. 2+4+6, if pivot is number - move it
            if (!isNaN(expression[pivot])) {
                if (expression[pivot - 1] == '-' || expression[pivot - 1] == '+' ) {
                    pivot = pivot - 1;
                } else {
                    pivot = pivot + 1;
                }
            }
            // if 2*3 - fine, if 1+2*3-4 - move it
            if (expression.length > 3 && (expression[pivot] == '*' || expression[pivot] == '/')) {
                pivot = Math.floor(pivot / 2);
            }
            var right = expression.slice(pivot + 1, expression.length);
            var left = expression.slice(0, pivot);

            //1 - 3*5 - 8 fails; calc (3*5 - 8) and then 1 - answer => fix
            if (expression[pivot] === '-') {
                expression[pivot] = '+';
                right[0] = -right[0];
            }
            
            var rightResult;
            if (right.length !== 1) {
                rightResult = calculateResult(right);
            } else {
                rightResult = right[0];
            }

            var leftResult;
            if (left.length !== 1) {
                leftResult = calculateResult(left);
            } else {
                leftResult = left[0];
            }

            return doMath(leftResult, expression[pivot], rightResult)
        }

        function doMath(left, operation, right) {
            switch(operation) {
                case '+':
                    return left + right;
                case '-':
                    return left - right;
                case '*':
                    return left * right;
                case '/':
                    return left / right;
            }
        }

    }

    this.savePsw = function(password){
        this.psw = this.encrypt(password)
    }

    this.encrypt = function(password) {
        return CryptoJS.SHA512(password).toString();
    }

    this.checkPsw = function(passwordToCheck) {
        return this.encrypt(passwordToCheck) === this.psw;
    }

}]);