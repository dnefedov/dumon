var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

function startExpress() {
	var port = 3000;
	var express = require('express');
	var app = express();
	app.use(express.static(__dirname + '/public'));
	app.listen(port, function() {
		console.log('Listening on port ' + port + '\n');
	});
}

gulp.task('default', function() {
	startExpress();
	plugins.livereload.listen();

    //Check for any changes and reload
	gulp.watch('public/**/*.*').on('change', function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', reloading');
		gulp.src(event.path).pipe(plugins.livereload());
	});
});