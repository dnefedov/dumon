# dumon

## Introduction
Dumon is a SPA that randomly populates simple math expression and results. 

## Technologies 
- AngularJs and Bootstrap used to populate UI and logic.
- gulp to automate tasks and run application. 
- NodeJS as a server.
- Bower as a dependency manager. 

## How to run
    $> sudo apt install npm
    $> sudo npm install -g bower
    $> (optional) sudo ln -s /usr/bin/nodejs /usr/bin/node
    $> sudo npm install -g gulp
    $> npm install
    $> bower install
    $> gulp

App will be available at http://localhost:3000

## License
MIT License

Copyright (c) [2016] [Dmitry Nefedov]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.